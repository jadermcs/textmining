FROM tensorflow/tensorflow:latest-gpu-py3-jupyter
WORKDIR /tf
COPY requirements.txt /tf/
RUN pip install -r requirements.txt
COPY . /tf/
EXPOSE 8888
