Text Mining for Legal Documents
---


Lucas Moreira, Jader Martins & Peng Yaohao

running:
```
docker build --tag=notebookgpu .
nvidia-docker run --rm -it -p 8888:8888 notebookgpu
```
